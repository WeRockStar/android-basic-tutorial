package com.example.werockstar.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class ContactSource {

    private SQLiteDatabase db;
    private ContactHelper contactHelper;
    private String colunm[] = {ContactHelper.CONTACT_ID, ContactHelper.CONTACT_NAME, contactHelper.CONTACT_TEL};

    public ContactSource(Context context) {
        contactHelper = new ContactHelper(context);
    }

    public void open() throws SQLException {
        db = contactHelper.getWritableDatabase();
    }

    public void close() throws SQLException {
        db.close();
    }

    public void save(Contact contact) {
        ContentValues values = new ContentValues();

        values.put(ContactHelper.CONTACT_NAME, contact.getName());
        values.put(ContactHelper.CONTACT_TEL, contact.getTel());

        db.insert(ContactHelper.TABLE_NAME, null, values);
    }

    public List<Contact> read() {
        List<Contact> list = new ArrayList<>();
        Cursor cursor = db.query(ContactHelper.TABLE_NAME, colunm, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Contact contact = new Contact(cursor.getString(1).toString(),
                    cursor.getString(2).toString());
            list.add(contact);
            cursor.moveToNext();
        }

        cursor.close();
        return list;
    }
}
