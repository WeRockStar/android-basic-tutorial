package com.example.werockstar.sqlite;

public class Contact {
    private int id;
    private String name;
    private String tel;

    public Contact(String name, String tel) {
        this.name = name;
        this.tel = tel;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTel() {
        return tel;
    }

    @Override
    public String toString() {
        return name;
    }
}
