package com.example.werockstar.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ContactHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "contacts.db";
    public static final String TABLE_NAME = "contact";

    //database column or field
    public static final String CONTACT_ID = "id";
    public static final String CONTACT_NAME = "name";
    public static final String CONTACT_TEL = "tel";

    public static final String COMMA = ",";


    public ContactHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + "("
                + CONTACT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + COMMA
                + CONTACT_NAME + " TEXT " + COMMA
                + CONTACT_TEL + " TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
