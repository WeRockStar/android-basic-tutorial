package com.example.werockstar.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    ContactSource source;
    EditText editTextName;
    EditText editTextTel;
    Button btnSave;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ContactAdapter adapter;

    Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialViews();
        setupViews();

        open();

        save();

        read();

    }

    private void read() {
        adapter = new ContactAdapter(source.read());
        recyclerView.setAdapter(adapter);
    }

    private void setupViews() {
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

    }


    private void save() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                String tel = editTextTel.getText().toString();

                contact = new Contact(name, tel);

                source.save(contact);

                adapter = new ContactAdapter(source.read());
                recyclerView.setAdapter(adapter);

                editTextTel.setText("");
                editTextName.setText("");
            }
        });
    }

    private void initialViews() {
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextTel = (EditText) findViewById(R.id.editTextTel);
        btnSave = (Button) findViewById(R.id.btnSave);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);

    }

    private void open() {
        source = new ContactSource(this);
        source.open();
    }
}