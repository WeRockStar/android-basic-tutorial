package com.example.werockstar.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {

    Switch switchSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        switchSound = (Switch) findViewById(R.id.switchSound);

        final SharedPreferences sf = getPreferences(Context.MODE_PRIVATE);
        int sound = sf.getInt("setting", 0);
        if (sound == 0) {
            switchSound.setChecked(false);
        } else {
            switchSound.setChecked(true);
        }

        switchSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchSound.isChecked()) {
                    SharedPreferences.Editor editor = sf.edit();
                    editor.putInt("setting", 1);
                    editor.commit();
                } else {
                    SharedPreferences.Editor editor = sf.edit();
                    editor.putInt("setting", 0);
                    editor.commit();
                }
            }
        });
    }
}
