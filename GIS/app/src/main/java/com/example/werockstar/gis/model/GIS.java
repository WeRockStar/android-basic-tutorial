package com.example.werockstar.gis.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GIS {

    @SerializedName("P_Name")
    private String name;

    @SerializedName("P_All")
    private String all;

    @SerializedName("P_existing")
    private String exist;


    public String getName() {
        return name;
    }

    public String getAll() {
        return all;
    }

    public String getExist() {
        return exist;
    }

}
