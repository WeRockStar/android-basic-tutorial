package com.example.werockstar.gis.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.werockstar.gis.R;
import com.example.werockstar.gis.model.GIS;

import java.util.List;

public class GISAdapter extends RecyclerView.Adapter<GISAdapter.GisViewHolder> {

    List<GIS> gisList;

    public GISAdapter(List<GIS> list) {
        this.gisList = list;
    }

    @Override
    public GisViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gis_item_row, parent, false);
        return new GisViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GisViewHolder holder, int position) {

        GIS gis = gisList.get(position);

        int result = Integer.parseInt(gis.getAll().toString()) - Integer.parseInt(gis.getExist().toString());

        holder.tvEx.setText(gis.getExist().toString());
        holder.tvAll.setText(gis.getAll().toString());
        holder.tvResult.setText(result + "");
        holder.tvName.setText(gis.getName().toString());
    }

    @Override
    public int getItemCount() {
        return gisList.size();
    }

    public class GisViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvAll;
        TextView tvEx;
        TextView tvResult;

        public GisViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvEx = (TextView) itemView.findViewById(R.id.tvEx);
            tvAll = (TextView) itemView.findViewById(R.id.tvAll);
            tvResult = (TextView) itemView.findViewById(R.id.tvResult);
        }
    }
}
