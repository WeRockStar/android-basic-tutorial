package com.example.werockstar.gis.services;

import com.example.werockstar.gis.model.GIS;


import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

public interface GisAPI {
    @GET("/select.php")
    void getInfo(Callback<List<GIS>> callback);
}
