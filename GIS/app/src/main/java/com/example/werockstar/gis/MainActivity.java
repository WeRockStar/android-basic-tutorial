package com.example.werockstar.gis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.werockstar.gis.adapter.GISAdapter;
import com.example.werockstar.gis.model.GIS;
import com.example.werockstar.gis.services.GisAPI;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    List<GIS> gisList;
    GISAdapter adapter;
    String BASE_URL = "http://spfinfo.esy.es";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialViews();

        setupViews();
    }

    private void initialViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    private void setupViews() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        GsonBuilder builder = new GsonBuilder();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(builder.create()))
                .build();

        GisAPI api = restAdapter.create(GisAPI.class);
        api.getInfo(new Callback<List<GIS>>() {
            @Override
            public void success(List<GIS> list, Response response) {

                adapter = new GISAdapter(list);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(MainActivity.class.getSimpleName(), error.getMessage().toString());
            }
        });
    }
}
