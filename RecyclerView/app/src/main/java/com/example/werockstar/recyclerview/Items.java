package com.example.werockstar.recyclerview;

public class Items {
    private String item;
    private String id;

    public Items(String item, String id) {
        this.item = item;
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public String getId() {
        return id;
    }
}
